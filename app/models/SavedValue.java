package models;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by Aleksandr on 21-Dec-16.
 */
@Entity
public class SavedValue {
    @Id
    private String clientKey;
    private String value;

    public SavedValue(String clientKey) {
        this.clientKey = clientKey;
    }

    public String getClientKey() {
        return clientKey;
    }

    public void setClientKey(String clientKey) {
        this.clientKey = clientKey;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
