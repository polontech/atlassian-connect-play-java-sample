package controllers;

import com.atlassian.connect.play.java.AC;
import com.atlassian.connect.play.java.auth.jwt.AuthenticateJwtRequest;
import com.atlassian.connect.play.java.controllers.AcController;
import com.atlassian.connect.play.java.token.CheckValidToken;
import com.avaje.ebean.Ebean;
import com.google.common.base.Supplier;
import models.SavedValue;
import play.data.DynamicForm;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;

public class Application extends Controller {

    public static Result index() {
        return AcController.index(
                AcController.home(),    // we keep the HTML home page from the module, as this is our documentation for now
                descriptorSupplier());  // serve the descriptor when accept header is 'application/json', try 'curl -H "Accept: application/json" http://localhost:9000'
    }

    public static Result descriptor() {
        return AcController.descriptor();
    }

    @AuthenticateJwtRequest
    public static Result getViewPage() {
        return ok(views.html.view.render(getSavedValueForHost().getValue()));
    }

    @AuthenticateJwtRequest
    public static Result getEditPage() {
        return ok(views.html.edit.render(getSavedValueForHost().getValue()));
    }

    @CheckValidToken
    public static Result postEditPage() {
        DynamicForm requestData = Form.form().bindFromRequest();
        String valueFromForm = requestData.get("text-input");
        SavedValue savedValue = getSavedValueForHost();
        savedValue.setValue(valueFromForm);
        Ebean.save(savedValue);
        return ok(views.html.edit.render(valueFromForm));
    }

    private static SavedValue getSavedValueForHost() {
        SavedValue savedValue = Ebean.find(SavedValue.class, AC.getAcHost().getKey());
        if (savedValue == null) {
            savedValue = new SavedValue(AC.getAcHost().getKey());
        }
        return savedValue;
    }

    private static Supplier descriptorSupplier() {
        return new Supplier() {
            public Result get() {
                return descriptor();
            }
        };
    }
}